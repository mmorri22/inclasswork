/**********************************************
* File: RouteDFS.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Contains an STL implementation of an Adjacency List
* graph Depth-First Search (no directions or weights) using C++ STL Vector 
*
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ g++ -g -std=c++11 -Wpedantic RouteDFS.cpp -o RouteDFS
mmorri22@remote303:~/testUserNDCSE/inclasswork/GraphSTL$ ./RouteDFS


**********************************************/

#include <iostream>
#include <vector>
#include <unordered_map>
#include <stack>
#include <fstream>

#define ROOT 0

// data structure to store graph edges
template<class T>
struct Edge {
	T src;
	T dest;
	int weight;
	
   template <typename U>      // all instantiations of this template are friends
   /********************************************
   * Function Name  : operator<<
   * Pre-conditions :  std::ostream&, const Edge<U>& 
   * Post-conditions: friend std::ostream&
   * Overloaded Friend Operator<< to print an edge  
   ********************************************/
   friend std::ostream& operator<<( std::ostream&, const Edge<U>& );

};

template <typename T>
std::ostream& operator<<( std::ostream& os, const Edge<T>& theEdge) {
   
   std::cout << "{" << theEdge.src << " " << theEdge.dest << " " << theEdge.weight << "} ";
   
   return os;
}


template<class T>
struct Vertex {
	T value;
	mutable std::vector< Edge <T> > edges;
	
	/********************************************
	* Function Name  : operator==
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded == operator to compare vertex value  
	********************************************/
	bool operator==(const Vertex<T>& rhs) const{
		
		return value == rhs.value;
		
	}
	
	/********************************************
	* Function Name  : operator<
	* Pre-conditions : const Vertex<T>& rhs
	* Post-conditions: bool
	*
	* Overloaded < operator to compare vertex values
	********************************************/
	bool operator<(const Vertex<T>& rhs) const{
		
		return value < rhs.value; 
		
	}
};

// class to represent a graph object
template<class T>
class Graph
{
	public:
		// construct a vectors to represent an adjacency list
		std::vector< Vertex<T> > adjList;
		
		// Hash Table to correlate Verticies with location in vector
		std::unordered_map< T, int > hashVerts;

		/********************************************
		* Function Name  : Graph
		* Pre-conditions : const std::vector< Edge<T> > &edges
		* Post-conditions: none
		*  
		* Graph Constructor
		********************************************/
		Graph(std::vector< Edge<T> > &edges, bool directional)
		{

			// add edges to the directed graph
			for (auto &edge: edges)
			{

				// Insert Origin 
				addEdge(edge);
				
				// Since this is undirected, put in opposite
				if(!directional){
					Edge<T> tempEdge = {edge.dest, edge.src, edge.weight};
					addEdge(tempEdge);
				}
				
			}
		}
		
		/********************************************
		* Function Name  : addEdge
		* Pre-conditions : Edge<T> edge
		* Post-conditions: none
		*  
		* Insert an edge into the graph
		********************************************/
		void addEdge(const Edge<T>& edge){
			
			// Create a temporary vertex with the source
			Vertex<T> tempVert;
			tempVert.value = edge.src;
			
			// Element was not found
			if(hashVerts.count(tempVert.value) == 0){
				
				// HashWords.insert( {wordIn, 1} );
				hashVerts.insert( {tempVert.value, adjList.size()} );
				
				// Inset the edge into the temporary vertex 
				// Serves as the first edge 
				tempVert.edges.push_back(edge);
				
				// Insert the vertex into the set 
				adjList.push_back(tempVert);
				
			}
			// Element was found!
			else{
				
				// Use the hash to get the location in adjList, then push onto the edges vector 
				adjList.at(hashVerts[tempVert.value]).edges.push_back(edge);
				
			}
		}
		
		/********************************************
		* Function Name  : returnHashLocation
		* Pre-conditions : T value
		* Post-conditions: int
		* 
		* Returns the location in the graph of the requested value 
		********************************************/
		int returnHashLocation(T value){
			
			return hashVerts[value];
		}
 
};

/********************************************
* Function Name  : printGraph
* Pre-conditions : const Graph<T>& graph
* Post-conditions: none
*  
* Prints all the elements in the graph 
********************************************/
template<typename T>
void printGraph(const Graph<T>& graph){
	
	std::cout << "Number of buckets is: " << graph.adjList.size() << std::endl;
	std::cout << "Origin: {Destination, Weight}" << std::endl;

	for(int iter = 0; iter < graph.adjList.size(); iter++){
		
		std::cout << graph.adjList.at(iter).value << ": ";
		
		for(int jter = 0; jter < graph.adjList.at(iter).edges.size(); jter++){
			
			std::cout << graph.adjList.at(iter).edges.at(jter);
			
		}
		
		std::cout << std::endl;
	}
	
	std::cout << std::endl;
	
}

/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, std::stack<T>& DFSOrder, T& findElem
* Post-conditions: none
* 
* Recursive basic function for DFS Traversal
* Stores traversal results in DFSOrder 
********************************************/
template<class T>
void GraphDFS(Graph<T>& graph, std::stack< T >& DFSOrder, T& findElem){
	
	// If the Graph has no elements, the return false 
	if(graph.adjList.size() == 0)
		return;
	
	// Create a Hash for visited Vertices 
	std::unordered_map<T, bool> visitedVerts;
	
	// If this returns true, then the element was found, add the root
	if(GraphDFS(graph, ROOT, DFSOrder, visitedVerts, findElem)){
		
		DFSOrder.push(graph.adjList.at(0).value);
	}

	
}


/********************************************
* Function Name  : GraphDFS
* Pre-conditions : Graph<T>& graph, int vecLoc, std::stack<T>& DFSOrder, 
*				   std::unordered_map<T, bool>& visitedVerts
* Post-conditions: none
*  
********************************************/
template<class T>
bool GraphDFS(Graph<T>& graph, int vecLoc, std::stack< T >&DFSOrder, 
	std::unordered_map<T, bool>& visitedVerts, T& findElem){
		
	// Mark the root node as visited 
	visitedVerts.insert( { graph.adjList.at(vecLoc).value, true } );
		
	// Get the vertex based on vecLoc 
	Vertex<T>* tempVert = &graph.adjList.at(vecLoc);
	
	// Base Case - Found node 
	if(tempVert->value == findElem){
		
		// No need to push onto the stack since the recursive call 
		// pushes all the found vertices.
		// Only need to push to the stack for the edge destination below
		
		return true;
	}
	
	for(int iter = 0; iter < tempVert->edges.size(); iter++){
		
		T tempDest = tempVert->edges.at(iter).dest;

		// If the edge destination is the element we are looking for 
		// Push onto the stack, and then return to the previous recursive call  
		if(tempDest == findElem){
			DFSOrder.push( tempDest );
			return true;
		}
		
		// Otherwise, the path continues
		if( visitedVerts.count( tempDest ) == 0 ){
			
			// Mark the destination vertex from the edge as visited 
			visitedVerts.insert( { tempDest, true } );
			
			// The recursive call found the node, then push current destination onto the stack 
			if(GraphDFS( graph, graph.returnHashLocation( tempDest ), DFSOrder, visitedVerts, findElem)){
				
				// If the edge destination is the element we are looking for 
				// Push onto the stack, and then return to the previous recursive call 
				DFSOrder.push( tempDest );
				return true;
			}
		}
	}
	
	return false;
	
}

/********************************************
* Function Name  : detectCycle
* Pre-conditions : Graph<T>& graph, std::vector<T>& DFSOrder
* Post-conditions: none
* 
* Recursive basic function for DFS Traversal
* Stores traversal results in DFSOrder 
********************************************/
template<class T>
void detectCycle(Graph<T>& graph, std::vector< Edge<T> >& DFSOrder){
	
	// Create the Hash for the visited results
	std::unordered_map<T, bool> visitedVerts;
	
	// Create the Hash for the visited results
	std::unordered_map<T, bool> cycleVerts;
	
	// Put the root node's first edge in the visited values
	visitedVerts.insert( { graph.adjList.at(ROOT).value , true} );
	
	// Call the recursive function
	detectCycle(graph, ROOT, DFSOrder, cycleVerts, visitedVerts);
	
}

template<class T>
/********************************************
* Function Name  : detectCycle
* Pre-conditions : Graph<T>& graph, int vecLoc, std::vector<T>& DFSOrder, std::unordered_map<T, bool>& visitedVerts
* Post-conditions: none
*  
********************************************/
void detectCycle(Graph<T>& graph, int vecLoc, std::vector< Edge<T> >& DFSOrder, 
				std::unordered_map<T, bool>& cycleVerts,
				std::unordered_map<T, bool>& visitedVerts){
	
	// Create a temporary vertex to simplify loop code
	// Make a pointer instead of copying entire Vertex 
	Vertex<T>* tempVert = &graph.adjList.at(vecLoc);
	
	// Put the root node's first edge in the visited values
	std::cout << "Inserting " << tempVert->value << " in cycle_Verts" << std::endl;
	cycleVerts.insert( { tempVert->value , true} );
	
	for(int iter = 0; iter < tempVert->edges.size(); iter++){
		
		// Create a temporary edge to simplify edge code 
		T tempDest = tempVert->edges.at(iter).dest;

		std::cout << "Origin: " << tempVert->value << " ";
		std::cout << "Destination: " << tempDest << std::endl;		
		if(cycleVerts.count( tempDest ) == 1){
			
			std::cout << "Cycle Detected ending in edge {";
			std::cout << tempVert->value << ", " << tempDest << "}" << std::endl;
			
			// Delete the edge 
			tempVert->edges.erase( tempVert->edges.begin()+iter );
			
			// Decrement iter 
			iter--;
		}
	
		// Check if the value for the destination of the iter edge is in the Hash
		// If 0, element was not found . Can continue DFS on this node		
		if(visitedVerts.count( tempDest ) == 0){
			
			// Put the visited value into the Hash
			visitedVerts.insert( { tempDest , true} );
			
			// Put the visited value into the Vector 
			DFSOrder.push_back( tempVert->edges.at(iter) );
			
			// Call the DFS for the destination vector 
			// Use the Graph's internal hash to get Vector location 
			
			if(graph.hashVerts.count( tempDest ) != 0 )
				detectCycle( graph, graph.returnHashLocation( tempDest ) , DFSOrder, cycleVerts, visitedVerts);
			
		}
	}
	
	// Now, we delete the edge from Cycle Check as we recurse back up the DFS
	std::cout << "Deleting " << tempVert->value << " from cycleVerts" << std::endl;
	cycleVerts.erase(tempVert->value);
}

/********************************************
* Function Name  : printDFS
* Pre-conditions : std::stack< T > DFSOrder
* Post-conditions: none
* 
* Prints the results of a DFS Traversal from startElem to findElem 
********************************************/
template<class T>
void printDFS(std::stack< T >& DFSOrder, T& startElem, T& findElem){
	
	std::cout << "The Depth-First Search Order from " << startElem << " to " << findElem << " is: ";
	
	// Inform the user if the element was not found 
	if(DFSOrder.empty()){
		
		std::cout << findElem << " was not found in the graph.";
	}
	
	// Stack is not empty -> Element was found 
	else{
			
		while(!DFSOrder.empty()){
			
			// Print the element at the top 
			std::cout << DFSOrder.top() << " ";
			
			// Remove the element from the stack 
			DFSOrder.pop();
			
		}
	
	}
	
	std::cout << std::endl << std::endl;
	
}

template<class T>
/********************************************
* Function Name  : printDFS
* Pre-conditions : std::vector< Edge<T> > DFSOrder
* Post-conditions: none
* 
* Prints the results of a DFS Traversal Topological Sort
********************************************/
void printDFS(std::vector< Edge<T> > DFSOrder){
	
	std::cout << "The Topological Sorted Order for the Graph is: ";
	
	for(int iter = 0; iter < DFSOrder.size(); iter++){
		
		std::cout << DFSOrder.at(iter) << " ";
	
	}
	
	std::cout << std::endl << std::endl;
	
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char** argv)
{
	
	// Run the DFS shown in class 
	// Similar to class, except there is an edge from 5 back to 'N' that will create a cycle
	// Another cycle between 'E' and 'D'
	std::vector< Edge<char> > edgeClass =
	{
		{'N', 'O', 1}, {'N', 'T', 1}, {'N', 'R', 1},
		{'O', 'R', 1}, {'O', 'E', 1}, {'O', 'N', 1},
		{'T', 'N', 1}, {'T', 'D', 1}, 
		{'R', 'D', 1}, 
		{'E', 'D', 1}, {'E', 'R', 1},
		{'D', 'E', 1}
	};
	
	// construct graph without directed edges 
	Graph<char> testGraph(edgeClass, true);
	
	// Print the Graph to the user 
	std::cout << "Initial Graph prior to edge detection:" << std::endl;
	printGraph(testGraph);
	
	std::vector< Edge<char> > intDFSOrder;
	
	// Run edge detection algorithm
	std::cout << "Running Edge Detection Algorithm..." << std::endl;
	detectCycle(testGraph, intDFSOrder);
	
	std::cout << std::endl << "Final Graph after edge detection:" << std::endl;
	printGraph(testGraph);
	
	return 0;
}
