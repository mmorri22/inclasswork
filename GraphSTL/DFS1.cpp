-------------------------------------
Run DFS In-Class Test Cases with undirected edges

Number of buckets is: 6
Origin: {Destination, Weight}
4: {4 20 1} {4 5 1} 
20: {20 4 1} {20 10 1} {20 30 1} 
5: {5 4 1} {5 10 1} 
10: {10 20 1} {10 5 1} {10 18 1} 
30: {30 20 1} {30 18 1} 
18: {18 10 1} {18 30 1} 

The Depth-First Search Order to find 18 is: 4 20 10 18 

The Depth-First Search Order to find 30 is: 4 20 10 18 30 

The Depth-First Search Order to find 20 is: 4 20 

The Depth-First Search Order to find 4 is: 4 

The Depth-First Search Order to find 17 is: 17 was not found in the graph.

The Depth-First Search Order to find 10 is: 4 20 10 

-------------------------------------
Run DFS In-Class Test Cases with directed edges

Number of buckets is: 5
Origin: {Destination, Weight}
4: {4 20 1} {4 5 1} 
20: {20 10 1} {20 30 1} 
5: {5 10 1} 
10: {10 18 1} 
30: {30 18 1} 

The Depth-First Search Order to find 18 is: 4 20 10 18 

The Depth-First Search Order to find 30 is: 4 20 30 

The Depth-First Search Order to find 20 is: 4 20 

The Depth-First Search Order to find 4 is: 4 

The Depth-First Search Order to find 17 is: 17 was not found in the graph.

The Depth-First Search Order to find 10 is: 4 20 10 

-------------------------------------
Run NOTRED Test Cases with undirected edges

Number of buckets is: 6
Origin: {Destination, Weight}
N: {N O 1} {N T 1} 
O: {O N 1} {O R 1} {O E 1} 
T: {T N 1} {T R 1} 
R: {R O 1} {R T 1} {R D 1} 
E: {E O 1} {E D 1} 
D: {D R 1} {D E 1} 

The Depth-First Search Order to find N is: N 

The Depth-First Search Order to find O is: N O 

The Depth-First Search Order to find T is: N O R T 

The Depth-First Search Order to find R is: N O R 

The Depth-First Search Order to find E is: N O R D E 

The Depth-First Search Order to find D is: N O R D 

The Depth-First Search Order to find Q is: Q was not found in the graph.

-------------------------------------
Run NOTRED Test Cases with directed edges

Number of buckets is: 5
Origin: {Destination, Weight}
N: {N O 1} {N T 1} 
O: {O R 1} {O E 1} 
T: {T R 1} 
R: {R D 1} 
E: {E D 1} 

The Depth-First Search Order to find N is: N 

The Depth-First Search Order to find O is: N O 

The Depth-First Search Order to find T is: N O R D T 

The Depth-First Search Order to find R is: N O R 

The Depth-First Search Order to find E is: N O E 

The Depth-First Search Order to find D is: N O R D 

The Depth-First Search Order to find Q is: Q was not found in the graph.

-------------------------------------
Run Classic Test Cases with undirected edges

Number of buckets is: 5
Origin: {Destination, Weight}
0: {0 1 1} {0 4 1} 
1: {1 0 1} {1 4 1} {1 2 1} {1 3 1} 
4: {4 0 1} {4 1 1} {4 3 1} 
2: {2 1 1} {2 3 1} 
3: {3 1 1} {3 2 1} {3 4 1} 

The Depth-First Search Order to find 0 is: 0 

The Depth-First Search Order to find 1 is: 0 1 

The Depth-First Search Order to find 1842 is: 1842 was not found in the graph.

The Depth-First Search Order to find 2 is: 0 1 4 3 2 

The Depth-First Search Order to find 3 is: 0 1 4 3 

The Depth-First Search Order to find 4 is: 0 1 4 

-------------------------------------
Run Classic Test Cases with directed edges

Number of buckets is: 4
Origin: {Destination, Weight}
0: {0 1 1} {0 4 1} 
1: {1 4 1} {1 2 1} {1 3 1} 
2: {2 3 1} 
3: {3 4 1} 

The Depth-First Search Order to find 0 is: 0 

The Depth-First Search Order to find 1 is: 0 1 

The Depth-First Search Order to find 1842 is: 1842 was not found in the graph.

The Depth-First Search Order to find 2 is: 0 1 2 

The Depth-First Search Order to find 3 is: 0 1 2 3 

The Depth-First Search Order to find 4 is: 0 1 4 

