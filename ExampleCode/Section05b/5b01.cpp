/**********************************************
* File: Elliott.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
* This file contains the driver for the 
* Elliott Wave Theory Problem Presented in 
* Section 5b CHANGE
**********************************************/
#include<iostream>

// Global value of 10 for Recursion
#define num 10

/********************************************
* Function Name  : Fib
* Pre-conditions : int n
* Post-conditions: double
*  
* Recursively determines nth Fibonacci value
********************************************/
double Fib(int n){
	if(n == 0)
		return 1;
	if(n == 1)
		return 1;
	return Fib(n-1) + Fib(n-2);
}

/********************************************
* Function Name  : FibDiv
* Pre-conditions : int n, int x
* Post-conditions: double
*  
* Calculates the division of the nth Fibonacci
* and the xth Fibonacci
********************************************/
double FibDiv(int n, int x){
	return Fib(n)/Fib(x);
}

/********************************************
* Function Name  : Elliott
* Pre-conditions : double Wave1
* Post-conditions: none
*  
* Calculates the Elliott Wave Numbers based
* on the input stock value
********************************************/
void Elliott2(double Wave1){

	std::cout << "Wave 1 = " << Wave1 << std::endl;
	std::cout << "Wave 2 Possibilities" << std::endl;
	std::cout << Wave1*FibDiv(1, 2) << std::endl;
	for(int j = 2; j <= 4; j++){
		std::cout << Wave1*(1 - FibDiv(num, num+j)) << std::endl;
	}
}

/********************************************
* Function Name  : Elliott3
* Pre-conditions : double Wave1
* Post-conditions: none
*  
* Calculates the 3rd Elliott Wave Numbers based
* on the input stock value
********************************************/
double Elliott3(double Wave1){

	std::cout << "Wave 3 Possibilities" << std::endl;
	double Wave3 = Wave1*FibDiv(num+1, num);
	std::cout << Wave3 << std::endl;
	
	return Wave3;
}

/********************************************
* Function Name  : Elliott4
* Pre-conditions : double Wave3
* Post-conditions: none
*  
* Calculates the 4th Elliott Wave Numbers based
* on the input stock value
********************************************/
void Elliott4(double Wave3){
	std::cout << "Wave 4 Possibilities" << std::endl;
	for(int j = 5; j >= 3; --j){
		std::cout << Wave3*FibDiv(num, num+j) << std::endl;
	}
}

/********************************************
* Function Name  : Elliott5
* Pre-conditions : double Wave1
* Post-conditions: none
*  
* Calculates the 5th Elliott Wave Numbers based
* on the input stock value
********************************************/
void Elliott5(double Wave1)
{
	std::cout << "Wave 5 Possibilities" << std::endl;
	double Wave5 = 0;
	for(int j = 1; j<=3; j++){
		Wave5 += Wave1*FibDiv(num, num+j);
		std::cout << Wave5 << std::endl;
	}
}

/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
* 
* Main Driver Function 
********************************************/
int main(int argc, char **argv){

	// Assume correct input for argv[1]
	double Wave1 = atof(argv[1]);

	// Run the Elliott Wave Function
	Elliott2(Wave1);
	Elliott4(Elliott3(Wave1));
	Elliott5(Wave1);
	
	return 0;
}

