/*************************************
 * File name: Section01_SLLNodeStruct.h 
 * Author: Matthew Morrison
 * Email: matt.morrison@nd.edu 
 * 
 * Contains the struct for a Node for a Doubly
 * Linked List 
 * ***********************************/

#ifndef SECTION01_SLLNODESTRUCT_H
#define SECTION01_SLLNODESTRUCT_H

template<class T>
struct Node {
    T     data;
    Node *next;
};

#endif
